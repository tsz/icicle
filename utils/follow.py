import time

# follow a file like tail -f. Ignores any line starting with '#'.
def follow(thefile):
    thefile.seek(0,2)
    while True:
        line = thefile.readline()
        if not line:
            time.sleep(0.1)
            continue
        if line.startswith('#'):
            continue
        yield line

if __name__ == "__main__":
    logfile = open("../LOG.txt", "r")
    loglines = follow(logfile)
    for line in loglines:
        print(line)
