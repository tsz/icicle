'''
Keithley2000 class for Keithley 2000 multimeter.
'''
import argparse
import time
import logging
from functools import update_wrapper

from .instrument import acquire_lock
from .visa_instrument import retry_on_fail_visa
from .scpi_instrument import SCPIInstrument, is_in, is_integer, is_numeric, truthy, verifier_or

logger = logging.getLogger(__name__)

class Keithley2000(SCPIInstrument, key='resource'):
    '''
    SCPIInstrument implementation for Keithley2000 multimeter.
    '''

    BAUD_RATE = 19200
    ''' Serial link Baud rate. '''
    TIMEOUT = 10000 # 10 seconds
    ''' Serial link timeout (ms). '''

    COM_SELFTEST = '*TST?'
    ''' Selftest SCPI command. '''
    COM_RESET = '*RST; STATUS:PRESET; *CLS'
    ''' Instrument Reset SCPI command. '''
    COM_CLEAR_TRACE = ':TRAC:CLE'
    ''' Clear trace SCPI command. '''
    COM_IMMEDIATE_TRIGGER = 'INIT:IMM'
    ''' Immediate Trigger SCPI command. '''

    SETTINGS = {
        'CONFIGURE': {
            'SET': ':CONF:{}',
            'QUERY': ':CONF?',
            'verifier': is_in('VOLT', 'VOLT:DC', 'VOLT:AC', 'CURR', 'CURR:DC', 'CURR:AC', 'RES', 'FRES')
        },
        'IDENTIFIER': {
            'QUERY': '*IDN?'
        },
        'READ': {
            'QUERY': ':READ?'
        },
        'SERVICE_REQUEST_ENABLE': {
            'SET': '*SRE {}',
            'QUERY': '*SRE?',
            'verifier': is_integer(min=1, max=1024)
        },
        'SAMPLE_COUNT': {
            'SET': ':SAMP:COUN {}',
            'QUERY': ':SAMP:COUN?',
            'verifier': is_integer(min=1, max=1024)
        },
        'MEASUREMENT_REGISTER': {
            'SET': ':STAT:MEAS:ENAB {}',
            'QUERY': ':STAT:MEAS:ENAB?',
            'verifier': is_integer(min=1, max=65535)
        },
        'SENSE_FUNCTION': {
            'SET': ':SENS:FUNC "{}"',
            'QUERY': ':SENS:FUNC?',
            'verifier': is_in('VOLT', 'VOLT:DC', 'VOLT:AC', 'CURR', 'CURR:DC', 'CURR:AC', 'RES', 'FRES')
        },
        'SENSE_CURRENT_INTEGRATION': {
            'SET': ':SENS:CURR:NPLC {:.2f}',
            'QUERY': ':SENS:CURR:NPLC?',
            'verifier': is_numeric(min=0.01, max=100)
        },
        'SENSE_VOLTAGE_INTEGRATION': {
            'SET': ':SENS:VOLT:NPLC {:.2f}',
            'QUERY': ':SENS:VOLT:NPLC?',
            'verifier': is_numeric(min=0.01, max=100)
        },
        'SENSE_CURRENT_AC_INTEGRATION': {
            'SET': ':SENS:CURR:AC:NPLC {:.2f}',
            'QUERY': ':SENS:CURR:AC:NPLC?',
            'verifier': is_numeric(min=0.01, max=100)
        },
        'SENSE_VOLTAGE_AC_INTEGRATION': {
            'SET': ':SENS:VOLT:AC:NPLC {:.2f}',
            'QUERY': ':SENS:VOLT:AC:NPLC?',
            'verifier': is_numeric(min=0.01, max=100)
        },
        'SENSE_RESISTANCE_INTEGRATION': {
            'SET': ':SENS:RES:NPLC {:.2f}',
            'QUERY': ':SENS:RES:NPLC?',
            'verifier': is_numeric(min=0.01, max=100)
        },
        'SENSE_FRESISTANCE_INTEGRATION': {
            'SET': ':SENS:FRES:NPLC {:.2f}',
            'QUERY': ':SENS:FRES:NPLC?',
            'verifier': is_numeric(min=0.01, max=100)
        },
        'SENSE_AVERAGING': {
            'SET': ':SENS:AVER {}',
            'QUERY': ':SENS:AVER?',
            'verifier': is_in('ON', 'OFF')
        },
        'SENSE_AVERAGING_COUNT': {
            'SET': ':SENS:AVER:COUN {}',
            'QUERY': ':SENS:AVER:COUN?',
            'verifier': is_integer(min=1, max=100)
        },
        'SENSE_AVERAGING_TYPE': {
            'SET': ':SENS:VOLT:NPLC {}',
            'QUERY': ':SENS:AVER:TCON?',
            'verifier': is_in('MOV', 'REP')
        },
        'INITIALISE_CONTINUOUS': {
            'SET': ':INIT:CONT {}',
            'QUERY': ':INIT:CONT?',
            'verifier': truthy(true_output=1, false_output=0)
        },
        'TRIGGER_COUNT': {
            'SET': ':TRIG:COUN {}',
            'QUERY': ':TRIG:COUN?',
            'verifier': verifier_or(is_integer(min=2, max=9999), is_in('INF'))
        },
        'TRIGGER_SOURCE': {
            'SET': ':TRIG:SOUR {}',
            'QUERY': ':TRIG:SOUR?',
            'verifier': is_in('IMM', 'TIM', 'MAN', 'BUS', 'EXT')
        },
        'TRIGGER_TIMER': {
            'SET': ':TRIG:TIM {}',
            'QUERY': ':TRIG:TIM?',
            'verifier': is_numeric()
        },
        'TRIGGER_DELAY': {
            'SET': ':TRIG:DEL {}',
            'QUERY': ':TRIG:DEL?',
            'verifier': is_numeric()
        },
        'TRACE_BUFFER_SIZE': {
            'SET': ':TRAC:POIN {}',
            'QUERY': ':TRAC:POIN?',
            'verifier': is_integer()
        },
        'TRACE_BUFFER_SOURCE': {
            'SET': ':TRAC:FEED {}',
            'QUERY': ':TRAC:FEED?',
            'verifier': is_in('SENS', 'SENS1', 'CALC', 'NONE') 
        },
        'TRACE_BUFFER_CONTROL_MODE': {
            'SET': ':TRAC:FEED:CONT {}',
            'QUERY': ':TRAC:FEED:CONT?',
            'verifier': is_in('NEXT', 'NEV') 
        },
        'AUTOZERO': {
            'SET': ':SYST:AZER:STAT {}',
            'QUERY': ':SYS:AZER:STAT?',
            'verifier': is_in('ON', 'OFF', 'ONCE')
        },
        'SENSE_DATA': {
            'QUERY': ':SENS:DATA?'
        },
        'TRACE_DATA': {
            'QUERY': ':TRAC:DATA?'
        },
        'MEASUREMENT_EVENT_REGISTER': {
                'QUERY': ':STAT:MEAS:EVEN?'
        },
        'DELAY': {
            'SET': ':TRIG:DEL {}',
            'QUERY': ':TRIG:DEL?',
            'verifier': is_numeric()
        },
    }
    ''' Settings dictionary with all Set/Query SCPI combinations. '''


    def __init__(self, resource = 'ASRL/dev/ttyUSB2::INSTR', reset_on_init = True):
        '''
        .. Warning: the ``resource`` keyword argument is mandatory and must be explicitly specified - failing to do so will result in an error since the Multiton metaclass on VisaInstrument masks this default value for ``resource``.

        :param resource: VISA Resource address. See VISA docs for more info.
        :param reset_on_init: Whether to reset Keithley2000 using COM_RESET command on device initialisation.
        '''
        super().__init__(resource, reset_on_init)

    @acquire_lock()
    def __enter__(self, recover_attempt = False):
        '''
        Initialises connection to Keithley 2000.

        :param recovery_attempt: Whether this is a recovery attempt by retry_on_fail.

        :return: `Keithley2000` object in activated state.
        '''
        super().__enter__(recover_attempt=recover_attempt, no_lock = True)
        if self._reset_on_init and not recover_attempt:
            self._selftest = self.selftest(no_lock = True)
            self.set('CONFIGURE', 'VOLT', no_lock=True)
            # Set default values here
            #self.set_all_line_integrations(no_lock=True)
            #self.set('SENSE_FRESISTANCE_INTEGRATION', 1, no_lock=True)
            #self.set('TRACE_BUFFER_SOURCE', 'SENS', no_lock=True)
            #self.set('TRIGGER_SOURCE', 'IMM', no_lock=True)
            #self.enable_register(no_lock=True)
            pass
        return self

    @acquire_lock()
    def __exit__(self, exception_type = None, exception_value = None, traceback = None, recover_attempt = False):
        '''
        Closes connection to Keithley 2000.

        :param recovery_attempt: Whether this is a recovery attempt by retry_on_fail.
        '''
        super().__exit__(exception_type, exception_value, traceback, no_lock=True)

    @acquire_lock()
    def selftest(self):
        '''
        Run self-test.
        
        :param no_lock: override `acquire_lock` (e.g. if lock already taken by function that `set`-call is nested within).

        :returns: result of `COM_SELFTEST` SCPI command.
        '''
        return self._instrument.query(type(self).COM_SELFTEST)
    
    @acquire_lock()
    @retry_on_fail_visa()
    def clear_trace(self):
        '''
        Clears measurement trace.
        
        :param no_lock: override `acquire_lock` (e.g. if lock already taken by function that `set`-call is nested within).
        :param attempts: how many retries to give `set` command.

        :returns: result of `COM_CLEAR_TRACE` SCPI command.
        '''
        return self._instrument.write(type(self).COM_CLEAR_TRACE)

    @acquire_lock()
    def enable_register(self):
        '''
        Enables measurement register.
        
        :param no_lock: override `acquire_lock` (e.g. if lock already taken by function that `set`-call is nested within).

        :returns: True-ish value on success.
        '''
        return (self.set('MEASUREMENT_REGISTER', 512, no_lock=True)
            and self.set('SERVICE_REQUEST_ENABLE', 1, no_lock=True))

    @acquire_lock()
    @retry_on_fail_visa()
    def immediate_trigger(self):
        '''
        Send immediate trigger.
        
        :param no_lock: override `acquire_lock` (e.g. if lock already taken by function that `set`-call is nested within).
        :param attempts: how many retries to give `set` command.

        :returns: number of bytes written to serial interface.
        '''
        return self._instrument.write(type(self).COM_IMMEDIATE_TRIGGER)
    
    def set_line_integration(self, what, line_integration_cycles=1, **kwargs):
        '''
        Set line integration cycles for given measurement type.
        
        :param what: one of 'VOLT:DC', 'VOLT:AC', 'CURR:DC', 'CURR:AC', 'RES', 'FRES'.
        :param line_integration_cycles: number of line integration cycles.
        :param no_lock: override `acquire_lock` (e.g. if lock already taken by function that `set`-call is nested within).
        :param attempts: how many retries to give `set` command.

        :returns: read-back value from set(...).
        '''
        assert(what in ('VOLT:DC', 'VOLT:AC', 'CURR:DC', 'CURR:AC', 'RES', 'FRES'))
        map = {'VOLT:DC':    'SENSE_VOLTAGE_INTEGRATION',
               'VOLT:AC': 'SENSE_VOLTAGE_INTEGRATION',
               'CURR:DC':    'SENSE_CURRENT_INTEGRATION',
               'CURR:AC': 'SENSE_CURRENT_INTEGRATION',
               'RES':     'SENSE_RESISTANCE_INTEGRATION',
               'FRES':    'SENSE_FRESISTANCE_INTEGRATION'}
        return self.set(map[what], line_integration_cycles, **kwargs)

    @acquire_lock()
    def set_all_line_integrations(self, line_integration_cycles=1):
        '''
        Set line integration cycles for all measurement types.
        
        :param line_integration_cycles: number of line integration cycles.
        :param no_lock: override `acquire_lock` (e.g. if lock already taken by function that `set`-call is nested within).

        :returns: read-back value from set(...).
        '''
        ret = True
        for setting in ('SENSE_VOLTAGE_INTEGRATION',
                        'SENSE_CURRENT_INTEGRATION',
                        'SENSE_RESISTANCE_INTEGRATION',
                        'SENSE_FRESISTANCE_INTEGRATION'):
            ret = self.set(setting, line_integration_cycles, no_lock=True) and ret
        return ret

    @acquire_lock()
    def measure_old(self, what, repetitions=1, delay=None, cycles=1.0, clear_trace=True):
        '''
        Old implementation of measure(). Doesn't work reliably - do not use.
        '''
        print(repetitions, what)
        assert(what in ('VOLT', 'VOLT:AC', 'CURR', 'CURR:AC', 'RES', 'FRES'))
        self.set('TRIGGER_SOURCE', 'IMM', no_lock=True)
        self.set_line_integration(what, cycles, no_lock=True)
        self.set('SAMPLE_COUNT', repetitions, no_lock=True)
        if repetitions > 1:
            self.set('TRACE_BUFFER_SIZE', repetitions, no_lock=True)
        self.set('SENSE_FUNCTION', f'"{what}"', no_lock=True)
        self.set('TRACE_BUFFER_CONTROL_MODE', 'NEXT', no_lock=True)
        if delay is not None:
            self.set('DELAY', delay, no_lock=True)

        # Trigger
        #self.immediate_trigger(no_lock=True)

        if delay is not None:
            wait_time = repetitions * (delay + cycles/60)
        else:
            wait_time = repetitions * cycles / 60
        logger.debug(f'Waiting {wait_time} seconds to collect data...')
        time.sleep(wait_time)

        rval = (
            [[float(aa) for aa in a.split(',') if aa != ''] for a in self.query('TRACE_DATA', no_lock=True).split(';')], 
            int(self.query('MEASUREMENT_EVENT_REGISTER', no_lock=True)))
        if clear_trace:
            self.clear_trace(no_lock=True)
        return rval
    
    @acquire_lock()
    def measure(self, what, repetitions=1, delay=None, cycles=1.0, clear_trace=True):
        '''
        Performs measurement of requested type.

        :param what: one of 'VOLT:DC', 'VOLT:AC', 'CURR:DC', 'CURR:AC', 'RES', 'FRES'.
        :param repetitions: how many measurements to make. Defaults to 1.
        :param delay: delay between measurements. Only used when repetitions > 1.
        :param cycles: number of line integration cycles for this measurment type. Defaults to 1.
        :param clear_trace: whether to clear trace after measurements. Defaults to True.
        :param no_lock: override `acquire_lock` (e.g. if lock already taken by function that `set`-call is nested within).

        :returns: list of measurement values (as floats).
        '''
        assert(what in ('VOLT:DC', 'VOLT:AC', 'CURR:DC', 'CURR:AC', 'RES', 'FRES'))
        if (self.query('CONFIGURE', no_lock=True).strip('"') != what 
                or int(self.query('INITIALISE_CONTINUOUS', no_lock=True)) == 1):
            self.set('CONFIGURE', what, no_lock=True)
        self.set_line_integration(what, cycles, no_lock=True)
        
        if repetitions > 1:
            rval = []
            if delay is None:
                delay = 1
            for i in range(repetitions):
                rval.append(float(self.query('READ', no_lock=True)))
                time.sleep(delay)
        else:
            rval = [float(self.query('READ', no_lock=True))]

        #if what == 'RES' or what =='FRES':
        #    self.set('SENSE_FUNCTION', 'VOLT:DC', no_lock=True)

        if clear_trace:
            self.clear_trace(no_lock=True)
        return rval

    def measure_single(self, what, clear_trace=True, **kwargs):
        '''
        Perform single measurement.

        :param what: one of 'VOLT:DC', 'VOLT:AC', 'CURR:DC', 'CURR:AC', 'RES', 'FRES'.
        :param clear_trace: whether to clear trace after measurements. Defaults to True.
        :param no_lock: override `acquire_lock` (e.g. if lock already taken by function that `set`-call is nested within).

        :returns: measured value (float).
        '''
        # uses lock from sub-call
        return self.measure(what, repetitions=1, delay=None, clear_trace=clear_trace, **kwargs)

# end class Keithley2000


Keithley2000.generate_methods()

    
