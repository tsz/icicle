'''
Relay Board CLI module. See ``relay_board --help`` for description, or read Click decorators.

This module is not explicitly documented here, as it is expected the CLI self-documentation should be sufficient.

See also the relay_board module.
'''

import click
import logging

from .relay_board import RelayBoard
from .cli_utils import with_instrument, print_output, verbosity

# ================== CLI METHODS ===================
# Below are script-like functions designed to be called to replace individual python scripts. They generally should:
# 1. instantiate and connect to instrument without resetting it
# 2. perform action
# 3. disconnect without ramping down or terminating instrument

@click.group()
@click.option('-v', '--verbose', count=True, help='Verbose output (-v = INFO, -vv = DEBUG)')
@click.option('-R', '--resource', metavar='TARGET', type=str, default='ASRL/dev/ttyUSB3::INSTR',
              help='VISA resource address (default: ASRL/dev/ttyUSB3::INSTR)')
@click.pass_context
def cli(ctx, resource, verbose, cls = RelayBoard):
    logging.basicConfig(level=verbosity(verbose))
    ctx.obj = cls(resource=resource, disconnect_on_exit=False)
# end def cli

@cli.command('off', help='Disconnect all pins.')
@with_instrument
def cli_off(instrument):
    return not instrument.off()

@cli.command('set_pin', help='Set connected pin PIN on relay board.')
@click.argument('pin', metavar='|'.join(RelayBoard.PIN_MAP.keys()),
                type=click.Choice(RelayBoard.PIN_MAP.keys()))
@with_instrument
@print_output
def cli_set_pin(instrument, pin):
    return instrument.set_pin(pin)

@cli.command('query_pin', help='Query connected pin on relay board.')
@with_instrument
@print_output
def cli_set_pin(instrument):
    return instrument.query_pin()

