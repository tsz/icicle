'''
Lauda class for Lauda chillers. 
'''
import argparse
import click
from functools import update_wrapper

from .instrument import acquire_lock
from .visa_instrument import retry_on_fail_visa
from .scpi_instrument import SCPIInstrument, is_in, is_integer, is_numeric, truthy

class Lauda(SCPIInstrument, key='resource'):
    '''
    SCPIInstrument implementation for Lauda chillers. 

    This class has been developed using the documentation for Lauda Eco Gold thermostats and tested using a Lauda Proline Kryomat RP 3090 CW chiller. This should work for other Lauda thermostats and chillers too.
    '''

    BAUD_RATE = 9600
    ''' Serial link Baud rate. '''

    TIMEOUT = 10000 # 10 seconds
    ''' Serial link timeout. '''

    COM_RESET = ''
    ''' Instrument Reset SCPI command. '''

    READ_TERMINATION = '\r\n'
    ''' Read termination characters '''

    WRITE_TERMINATION = '\r\n'
    ''' Write termination characters '''
    
    SETTINGS = {
        'START': {
            'SET': 'START'
        },
        'STOP': {
            'SET': 'STOP'
        },
        'OPERATION': {
            'QUERY': 'IN_MODE_02'
        },
        'STATUS': {
            'QUERY': 'STATUS'
        },
        'DIAGNOSTICS': {
            'QUERY': 'STAT'
        },
        'TEMPERATURE_TARGET': {
            'SET': 'OUT_SP_00_{}',
            'QUERY': 'IN_SP_00',
            'verifier': is_numeric(min=-90., max=200.),
        },
        'TEMPERATURE_BATH': {
            'QUERY': 'IN_PV_00'
        },
        'TEMPERATURE_EXTERNAL': {
            'QUERY': 'IN_PV_01'
        },
        'TEMPERATURE_REFERENCE': {
            'SET': 'OUT_MODE_01_{}',
            'QUERY': 'IN_MODE_01',
            'verifier': is_in(0, 1, '0', '1'),
        },
        'TYPE': {
            'QUERY': 'TYPE'
        },
        'COMMAND': {
            'SET': '{}'
        },

    }
    ''' Settings dictionary with all Set/Query SCPI combinations. '''


    def __init__(self, resource = 'ASRL/dev/ttyUSB0::INSTR', reset_on_init = True, off_on_close=False):
        '''
        .. Warning: the ``resource`` keyword argument is mandatory and must be explicitly specified - failing to do so will result in an error since the Multiton metaclass on VisaInstrument masks this default value for ``resource``.

        :param resource: VISA Resource address. See VISA docs for more info.
        :param reset_on_init: Whether to reset Lauda using COM_RESET command on device initialisation.
        :param off_on_close: Whether to turn off outputs on __exit__.
        '''
        super().__init__(resource, reset_on_init)
        self._off_on_close = off_on_close

    @acquire_lock()
    def __enter__(self, recover_attempt = False):
        '''
        Initialises connection to Lauda.

        :param recovery_attempt: Whether this is a recovery attempt by retry_on_fail.

        :return: `Lauda` object in activated state.
        '''
        super().__enter__(recover_attempt=recover_attempt, no_lock = True)
        if self._reset_on_init and not recover_attempt:
            # Set default values here
            pass
        return self

    @acquire_lock()
    def __exit__(self, exception_type = None, exception_value = None, traceback = None, recover_attempt = False):
        '''
        Closes connection to Lauda.

        :param recovery_attempt: Whether this is a recovery attempt by retry_on_fail.
        '''
        if self._off_on_close and not recover_attempt:
            # Should be safe for pretty much anything
            self.off(output, no_lock=True)
        super().__exit__(exception_type, exception_value, traceback, no_lock=True)

# end class Lauda


Lauda.generate_methods()
