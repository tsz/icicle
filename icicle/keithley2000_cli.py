'''
Keithley2000 CLI module. See ``keithley2000 --help`` for description, or read Click decorators.

This module is not explicitly documented here, as it is expected the CLI self-documentation should be sufficient.

See also the keithley2000 module.
'''

import click
import logging

from .keithley2000 import Keithley2000
from .cli_utils import with_instrument, print_output, verbosity

# ================== CLI METHODS ===================
# Below are script-like functions designed to be called to replace individual python scripts. They generally should:
# 1. instantiate and connect to instrument without resetting it
# 2. perform action
# 3. disconnect without ramping down or terminating instrument

@click.group()
@click.option('-v', '--verbose', count=True, help='Verbose output (-v = INFO, -vv = DEBUG)')
@click.option('-R', '--resource', metavar='TARGET', type=str, default='ASRL/dev/ttyUSB2::INSTR',
              help='VISA resource address (default: ASRL/dev/ttyUSB2::INSTR)')
@click.pass_context
def cli(ctx, resource, verbose, cls = Keithley2000):
    logging.basicConfig(level=verbosity(verbose))
    ctx.obj = cls(resource=resource, reset_on_init=False)
# end def cli

@cli.command('reset', help='Reset instrument. Will safely ramp down and perform reset.')
@with_instrument
def cli_reset(instrument):
    # rather than just calling reset(), regenerate context handler to get nice default values from above
    # also currently enforces safe ramp down procedure
    instrument._reset_on_init = True
    instrument.__exit__(recover_attempt=False)
    instrument.__enter__(recover_attempt=False)
    instrument._reset_on_init = False
    return True

@cli.command('identify', help='Identify instrument')
@with_instrument
@print_output
def cli_identify(instrument):
    return instrument.identify()

@cli.command('selftest', help='Perform instrument self-test')
@with_instrument
@print_output
def cli_selftest(instrument):
    return instrument.selftest()

@cli.command('measure', help='Measure voltage (V), current (I), resistance (R) or four-wire resistance (FR). Voltage and current measurement also support AC mode (V:AC, I:AC).')
@click.argument('what', metavar='V|V:AC|I|I:AC|R|FR', 
        type=click.Choice(['V', 'V:AC', 'I', 'I:AC', 'R', 'FR', 'v', 'v:ac', 'i', 'i:ac', 'r', 'fr', 'VOLT', 'VOLT:AC', 'CURR', 'CURR:AC', 'RES', 'FRES', 'volt', 'volt:ac', 'curr', 'curr:ac', 'res', 'fres', 'voltage', 'voltage:ac', 'current', 'current:ac', 'resistance', 'fresistance']))
@click.option('-c', '--cycles', metavar='CYCLES', type=float, default=1.0,
              help='line integration cycles (in range 0.01 to 10; defaults to 1)')
@click.option('-r', '--repetitions', metavar='REPETITIONS', type=int, default=1,
              help='repetitions of this measurement (defaults to 1)')
@click.option('-d', '--delay', metavar='DELAY', type=float, default=None,
              help='delay between repetitions (seconds; requires repetitions > 1)')
@with_instrument
@print_output
def cli_measure(instrument, what, cycles=1.0, repetitions=1, delay=None):
    if what in ['V', 'v', 'VOLT', 'volt', 'voltage']:
        what = 'VOLT:DC'
    elif what in ['V:AC', 'v:ac', 'VOLT:AC', 'volt:ac', 'voltage:ac']:
        what = 'VOLT:AC'
    elif what in ['I', 'i', 'CURR', 'curr', 'current']:
        what = 'CURR:DC'
    elif what in ['I:AC', 'i:ac', 'CURR:AC', 'curr:ac', 'current:ac']:
        what = 'CURR:AC'
    elif what in ['R', 'r', 'RES', 'res', 'resistance']:
        what = 'RES'
    elif what in ['FR', 'fr', 'FRES', 'fres', 'fresistance']:
        what = 'FRES'
    return instrument.measure(what, repetitions, delay, cycles)


# ======== LOW-LEVEL COMMANDS - Take care if using these =========

@cli.command('set', help='Set configuration field FIELD to value VALUE (see keithley2410.py for config table).')
@click.argument('field', metavar='FIELD', type=str)
@click.argument('value', metavar='VALUE', type=str)
@with_instrument
@print_output
def cli_set(instrument, field, value):
    return instrument.set(field, value)

@cli.command('query', help='Query configuration field FIELD (see keithley2410.py for config table).')
@click.argument('field', metavar='FIELD', type=str)
@with_instrument
@print_output
def cli_query(instrument, field):
    return instrument.query(field)

@cli.command('scpi_write', help='Send SCPI command COMMAND. [LOW-LEVEL COMMAND; use only if you really know what you\'re doing...]')
@click.argument('command', metavar='COMMAND', type=str, nargs=-1)
@with_instrument
@print_output
def cli_scpi_write(instrument, command):
    return instrument._instrument.write(' '.join(command))

@cli.command('scpi_query', help='Send SCPI command COMMAND, and immediately read response. [LOW-LEVEL COMMAND; use only if you really know what you\'re doing...]')
@click.argument('command', metavar='COMMAND', type=str, nargs=-1)
@with_instrument
@print_output
def cli_scpi_query(instrument, command):
    return instrument._instrument.query(' '.join(command))

@cli.command('scpi_read', help='Attempt to read on SCPI serial line. [LOW-LEVEL COMMAND; use only if you really know what you\'re doing...]')
@with_instrument
@print_output
def cli_scpi_read(instrument):
    return instrument._instrument.read()
