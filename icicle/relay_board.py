'''
RelayBoard class for ETHZ Relayboard (Vasilije Perovic design).
'''

import time
from .instrument import acquire_lock
from .visa_instrument import VisaInstrument, retry_on_fail_visa

class RelayBoard(VisaInstrument, key='resource'):
    '''
    VisaInstrument implementation for ETHZ Relayboard (Vasilije Perovic design). Additional (fake) set(), query() commands have been added to make this a psuedo-implementation of SCPIInstrument (and match behaviour of said interface). 
    '''

    BAUD_RATE = 9600
    ''' Serial link Baud rate. '''
    TIMEOUT = 50000 # 5 seconds
    ''' Serial link timeout (ms). '''

    PIN_MAP = {
        'VDDD_ROC2': 	'a',
        'VDDA_ROC2': 	'b',
        'VDDD_ROC3': 	'c',
        'VDDA_ROC3': 	'd',
        'VDDD_ROC0': 	'e',
        'VDDA_ROC0': 	'f',
        'VDDA_ROC1': 	'g',
        'VDDD_ROC1': 	'h',
        'GND':		    'i',
        'NTC': 	        'j',
        'OFF':          'x'
    }
    ''' Pin map for Arduino pin names. Must match arduino firmware version. '''
    
    def __init__(self, resource='ASLR/dev/ttyUSB3::INSTR', disconnect_on_exit=False):
        '''
        .. Warning: the ``resource`` keyword argument is mandatory and must be explicitly specified - failing to do so will result in an error since the Multiton metaclass on VisaInstrument masks this default value for ``resource``.

        :param resource: VISA Resource address. See VISA docs for more info.
        :param disconnect_on_exit: Whether to set connected pin to `OFF` on device close.
        '''
        super().__init__(resource)
        self._disconnect_on_exit = disconnect_on_exit

    @acquire_lock()
    def __enter__(self, recover_attempt=False):
        '''
        Initialises connection to relay board.

        :param recovery_attempt: Whether this is a recovery attempt by retry_on_fail.

        :return: `RelayBoard` object in activated state.
        '''
        super().__enter__(self, no_lock=True)
        self._instrument.read_termination = ''
        self._instrument.write_termination = ''
        time.sleep(1)
        return self

    @acquire_lock()
    def __exit__(self, exception_type = None, exception_value = None, traceback = None, recover_attempt = False):
        '''
        Closes connection to relay board.

        :param recovery_attempt: Whether this is a recovery attempt by retry_on_fail.
        '''
        if not recover_attempt and self._disconnect_on_exit:
            self.off(no_lock=True)
        super().__exit__(exception_type, exception_value, traceback, no_lock=True)

    @acquire_lock()
    @retry_on_fail_visa()
    def set_pin(self, pin):
        '''
        Set currently connected pin on relay board to `pin`.

        :param pin: pin name in `PIN_MAP`. 
        :param no_lock: override `acquire_lock` (e.g. if lock already taken by function that `set`-call is nested within).
        :param attempts: how many retries to give `set` command.

        :returns: result returned by relay board (usually pin number that has been connected/powered).
        '''
        if pin in type(self).PIN_MAP:
            return self._instrument.query(self.PIN_MAP[pin])
        else:
            raise RuntimeError(f'Invalid pin name: {pin}. Valid pin names: {",".join(type(self).PIN_MAP.keys())}')
    
    def status(self, **kwargs):
        '''
        Queries currently connected pin, returns raw result.

        .. warning: Generally one should use query_pin instead since this returns the raw numeric result, not the converted pin name.

        :param no_lock: override `acquire_lock` (e.g. if lock already taken by function that `set`-call is nested within).
        :param attempts: how many retries to give `set` command.

        :returns: result returned by relay board (usually pin number that has been connected/powered).
        '''
        return self.query_pin(**kwargs)

    def set(self, setting, value, **kwargs):
        '''
        "Fake" entry point to extend common interface as much as possible.
        
        Set currently connected pin on relay board to `pin`. See set_pin().
        
        :param setting: must be `PIN`.
        :param value: pin name in `PIN_MAP`. 
        :param no_lock: override `acquire_lock` (e.g. if lock already taken by function that `set`-call is nested within).
        :param attempts: how many retries to give `set` command.

        :returns: result returned by relay board (usually pin number that has been connected/powered).
        '''
        # uses lock from sub-command - pass kwargs
        if setting != 'PIN':
            raise RuntimeError(f'Setting {setting} not found for device {type(self).name}, or query-only. Cannot call set().')
        else:
            return self.set_pin(value, **kwargs)

    @acquire_lock()
    @retry_on_fail_visa()
    def query_pin(self):
        '''
        Query currently connected pin, return pin name as in `PIN_MAP`. 
        
        :param no_lock: override `acquire_lock` (e.g. if lock already taken by function that `set`-call is nested within).
        :param attempts: how many retries to give `set` command.

        :returns: pin name as in `PIN_MAP`.
        '''
        response = self._instrument.query('?')
        return next(key for key, value in type(self).PIN_MAP.items() if value == response.strip())

    def set(self, setting, value, **kwargs):
        # "Fake" entry point to extend common interface as much as possible
        # uses lock from sub-command - pass kwargs
        if setting != 'PIN':
            raise RuntimeError(f'Setting {setting} not found for device {type(self).name}, or query-only. Cannot call set().')
        else:
            return self.set_pin(value, **kwargs)

    def query(self, setting, **kwargs):
        '''
        "Fake" entry point to extend common interface as much as possible.
        
        Query currently connected pin. See query_pin().
        
        :param setting: must be `PIN`.
        :param no_lock: override `acquire_lock` (e.g. if lock already taken by function that `set`-call is nested within).
        :param attempts: how many retries to give `set` command.

        :returns: pin name as in `PIN_MAP`.
        '''
        # "Fake" entry point to extend common interface as much as possible
        # uses lock from sub-command - pass kwargs
        if setting != 'PIN':
            raise RuntimeError(f'Setting {setting} not found for device {type(self).name}, or set-only. Cannot call query().')
        else:
            return self.query_pin(**kwargs)

    def off(self, **kwargs):
        '''
        Set connected pin to `OFF`. 

        :param no_lock: override `acquire_lock` (e.g. if lock already taken by function that `set`-call is nested within).
        :param attempts: how many retries to give `set` command.

        :returns: result returned by relay board (usually pin number that has been connected/powered).
        '''
        # uses lock from sub-command - pass kwargs
        return int(self.set_pin('OFF', **kwargs)) == 23

# end class RelayBoard

