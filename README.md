ICICLE - Instrument Control Interface and Commands Library developed @ETHZ
==========================================================================

This library is designed to provide a lightweight yet robust set of classes and
command line tools for DCS and instrument control.

Read the full documentation at https://icicle.docs.cern.ch/.

The following devices are currently supported:
- Rhode&Schwarz/Hameg HMP4040 LV power supply: `hmp4040`.
- TTI MX100TP-class LV power supply: `tti`.
- Keithley 2000 multimeter: `keithley2000`.
- Keithley 24X0-class source-measure unit: `keithley2410`.
- Gulmay MP1 xray controller: `mp1`.
- ETHZ SLDO probe card controller board (arduino; a. la. Vasilije Perovic): 
  `relay_board`

A super-device implementation for a standard module testing setup consisting of 
LV, HV, relay board, and multimeter (with interlock features) is provided: 
`instrument_cluster`.

## Instrument Last-Known-Tested Versions

Documented here is the last version that instruments have been explicitly tested on 
(since many developers don't have access to all types of instruments and cannot
test possibly breaking changes to all). Patch-level changes should not break
existing instruments or functionality. 

| Instrument          | LKT Version | Tested By        | Date     |
| ------------------- | ----------- | ---------------- | -------- |
| `HMP4040`           | 1.0.0       | D. Bacher        | -        |
| `Keithley2000`      | 1.0.0       | D. Bacher        | -        |
| `Keithley2410`      | 1.0.0       | D. Bacher        | -        |
| `MP1`               | 1.1.0       | S. Koch          | 04.04.22 |
| `Relay\_Board`      | 1.0.0       | D. Ruini         | -        |
| `TTI`               | 1.0.0       | D. Bacher        | -        |
| `Lauda`             | 1.2.0       | F. Guescini      | 22.06.30 |
|                     |             |                  |          |
| `InstrumentCluster` | <1.0.0      | S. Koch          | -        |
| `dirigent`          | <1.0.0      | S. Koch          | -        |

## Installation

Python >= 3.6 is required. Python 3.9 or higher is recommended (and is required 
for other parts of the dirigent module testing suite).

It is recommended to install into a virtual environment - for example:
```
python3 -m venv python3-env
. python3-env/bin/activate
```

Installation is performed using the `setup.py` script:
```
python3 setup.py install
```

If you would like to be able to modify the source files, subsitute `develop` for 
`install`.

## Building API Documentation

Building API docs requires both the `icicle` package and the `sphinx` python 
package to be installed (API docs are generated by reading python sources):
```
. path/to/virtual/env/bin/activate
python3 -m pip install sphinx
```

Then build using `sphinx-build` (you can subsitute latex for html to generate 
latex sources):
```
cd docs
sphinx-build -b html source build
```

Then point your browser at index.html in the `docs/build` directory, and enjoy. 

## Command Line Interface - Usage

For each device type, simply type `<device> --help` for a full list of commands 
and arguments. For example:
```
$ hmp4040 --help

Usage: hmp4040 [OPTIONS] COMMAND [ARGS]...

Options:
  -v, --verbose          Verbose output (-v = INFO, -vv = DEBUG)  [x>=0]
  -R, --resource TARGET  VISA resource address (default:
                         ASRL/dev/ttyACM0::INSTR)
  --help                 Show this message and exit.

Commands:
  activate      Activate output for channel CHANNEL (1, 2, 3, or 4)
  activated     Read output activation status for channel CHANNEL (1, 2,...
  current       Read current currently set for channel CHANNEL (without...
  deactivate    Deactivate output for channel CHANNEL (1, 2, 3 or 4)
  identify      Identify instrument
  measure       Measure voltage (in V) and current (in A) currently...
  monitor       Measure voltage (in V) and current (in A) currently...
  off           Turn general output OFF
  on            Turn general output ON
  ovp           Read overvoltage protection currently set for channel...
  query         Query configuration field FIELD (see hmp4040.py for...
  ramp_current  Ramp channel CHANNEL to target current TARGET (A).
  ramp_voltage  Ramp channel CHANNEL to target voltage TARGET (V).
  reset         Reset instrument.
  scpi_query    Send SCPI command COMMAND, and immediately read response.
  scpi_read     Attempt to read on SCPI serial line.
  scpi_write    Send SCPI command COMMAND.
  set           Set configuration field FIELD to value VALUE (see...
  status        Read general output state (ON/OFF)
  voltage       Read voltage currently set for channel CHANNEL (without...
```

Individual commands can also be combined with `--help` for further information:
```
$ hmp4040 ovp --help
Usage: hmp4040 ovp [OPTIONS] [CHANNEL] OVP

  Read overvoltage protection currently set for channel CHANNEL (without OVP
  argument), or set OVP (V) for channel CHANNEL (1, 2, 3, or 4)

Options:
  --help  Show this message and exit.
```

## Graphical Monitoring

The library offers a graphical display of PSU outputs (currently only TTI MX100TP-class and Keithley24X0-class) using Eclipse-Mosquitto, Telegraf, Influxdb and Grafana, all contained in Docker containers.

### Prerequisites

To use the graphical monitoring, Docker and Docker-Compose need to be installed. For installation guides, see:
- [Docker](https://docs.docker.com/engine/install/)
- [Docker-Compose](https://docs.docker.com/compose/install/)

### Setup

Change into the `monitoring` directory and run docker-compose using the `docker-compose.yml` configuration file:
```
cd monitoring
docker-compose -f docker-compose.yml up -d
```

This will start Docker containers of Eclipse-Mosquitto, Telegraf, IndluxDB and Grafana. The Grafana docker can be accessed in your browser under: `http://localhost:3000`

Running `<device> monitor -m` on the same machine should confirm the connection to the MQTT broker and will send the measurements to the InfluxDB database, from where they can be accessed with Grafana. For example:
```
$ tti monitor -m ALL
Connected to MQTT broker!
# MONITORING_START 01/01/22
# Time              Voltage1 (V)        Current1 (A)        Voltage2 (V)        Current2 (A)     
01/01/22 12:34:56   1.234               0.4567              -9.876              -4.321
```

For more information on how to configure the monitoring and Grafana, see the full documentation at [https://icicle.docs.cern.ch/](https://icicle.docs.cern.ch/).
