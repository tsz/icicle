﻿instrument
==========

.. currentmodule:: icicle.instrument





.. automodule:: icicle.instrument
   :ignore-module-all:
 
   
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
   
      ~acquire_lock
      ~retry_on_fail
   
   

   
   
   .. rubric:: Classes

   .. autosummary::
   
      ~Instrument
      ~LOCK_NOT_ACQUIRED
      ~Multiton
      ~Singleton
   
   

   
   
   .. rubric:: Exceptions

   .. autosummary::
   
      ~InstrumentTimeoutError
   
   










acquire\_lock
-------------

.. autofunction:: acquire_lock

retry\_on\_fail
---------------

.. autofunction:: retry_on_fail












Instrument
----------

.. autoclass:: Instrument
   :members:       
   :undoc-members:    
   :show-inheritance:     

   
   .. automethod:: __init__

   
   

   
   
   

LOCK\_NOT\_ACQUIRED
-------------------

.. autoclass:: LOCK_NOT_ACQUIRED
   :members:       
   :undoc-members:    
   :show-inheritance:     

   
   .. automethod:: __init__

   
   

   
   
   

Multiton
--------

.. autoclass:: Multiton
   :members:       
   :undoc-members:    
   :show-inheritance:     

   
   .. automethod:: __init__

   
   

   
   
   

Singleton
---------

.. autoclass:: Singleton
   :members:       
   :undoc-members:    
   :show-inheritance:     

   
   .. automethod:: __init__

   
   

   
   
   








InstrumentTimeoutError
----------------------

.. autofunction:: InstrumentTimeoutError

