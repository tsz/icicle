﻿instrument\_cluster
===================

.. currentmodule:: icicle.instrument_cluster





.. automodule:: icicle.instrument_cluster
   :ignore-module-all:
 
   
   
   

   
   
   

   
   
   .. rubric:: Classes

   .. autosummary::
   
      ~DummyInstrument
      ~InstrumentCluster
      ~InstrumentNotInstantiated
   
   

   
   
   .. rubric:: Exceptions

   .. autosummary::
   
      ~BadStatusForOperationError
      ~MissingRequiredInstrumentError
   
   


















DummyInstrument
---------------

.. autoclass:: DummyInstrument
   :members:       
   :undoc-members:    
   :show-inheritance:     

   
   .. automethod:: __init__

   
   

   
   
   

InstrumentCluster
-----------------

.. autoclass:: InstrumentCluster
   :members:       
   :undoc-members:    
   :show-inheritance:     

   
   .. automethod:: __init__

   
   

   
   
   

InstrumentNotInstantiated
-------------------------

.. autoclass:: InstrumentNotInstantiated
   :members:       
   :undoc-members:    
   :show-inheritance:     

   
   .. automethod:: __init__

   
   

   
   
   








BadStatusForOperationError
--------------------------

.. autofunction:: BadStatusForOperationError

MissingRequiredInstrumentError
------------------------------

.. autofunction:: MissingRequiredInstrumentError

