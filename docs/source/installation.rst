.. _installation:

Installation
============

Python 3.6 at minimum is required for the installation of `icicle`; 3.9 or greater is recommended to ensure cross-compatibility with `dirigent` and the rest of the ETHZ module testing framework.


Environment
-----------
It is recommended to install Icicle and associated dependencies in a separate python environment. One may use Python virtualenvs or anaconda environments for this purpose - it makes litte practical difference. An example of how to set up a python virtualenv for this purpose is below::
    
    cd /path/to/cloned/repository/
    python3 -m venv py3_environment
    . py3_environment/bin/activate


Dependencies
------------
Most package dependencies are automatically installed by `pip`; no specific action is required.


``setup.py``
------------

Finally, the `icicle` package may be installed as a developer installation using ``setup.py``, allowing changes to be made to the source code, or for new instruments to be added to the ``./icicle`` package::

    python3 setup.py develop

Alternatively, a static production installation can be done using::

    python3 setup.py install

